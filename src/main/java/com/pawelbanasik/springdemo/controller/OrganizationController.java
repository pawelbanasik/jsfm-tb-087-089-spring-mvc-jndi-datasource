
package com.pawelbanasik.springdemo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

import com.pawelbanasik.springdemo.domain.Organization;
import com.pawelbanasik.springdemo.service.OrganizationService;

@Controller
public class OrganizationController {
	
	// should have been interface but he didn't do it for simplicity
	@Autowired
	private OrganizationService organizationService;

	// all default go here
	@RequestMapping
	public String listOrganizationUsingSQLTag() {
		return "listOrganization1";
	}
	
	// except this one
	@RequestMapping("/service")
	public String listOrganizationUsingService(Model model) {
		List<Organization> orgs = organizationService.getOrgList();
		model.addAttribute("orgList", orgs);
		return "listOrganization2";
	}

}
